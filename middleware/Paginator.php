<?php

class Paginator
{

    public function __invoke($request, $response, $next)
    {
        if (isset($request->getQueryParams()['page'])) {
            $cur_page = $request->getQueryParams()['page'];
            Illuminate\Pagination\Paginator::currentPageResolver(function() use ($cur_page) {
                return $cur_page;
            });
        }
        return $response = $next($request, $response);

    }
}