<?php

use Tracy\Debugger;
class Tracy
{

    private $container;

    public function __construct($container) {
        $this->container = $container;
    }

    public function __invoke($request, $response, $next)
    {
        $user = $this->container['sentinel']->check();
        if ($user) {
            Debugger::enable(false);
        }
        $response = $next($request, $response);
        return $response;
    }
}