$( document ).ready(function() {
    $('.addtask').click(function() {
        $('#name').val('');
        $('.popup-task .add').attr('data-type', 'POST');
        $('.popup-task').show('fast');
    });

    $('.list').on('click', '.task', function() {
        var id = $(this).attr('id');
        var list = $(this).closest('.list').attr('data-id');
        $('.popup-task .add').attr('data-type', 'PUT');
        $('.popup-task .add').attr('data-taskid', id);
        $('.popup-task .comment').attr('data-taskid', id);
        $('.popup-task input.card_id').attr('value', id);        
        $('.popup-task .delete').css('display', 'inline-block');
        $('.popup-task .delete').attr('id', $(this).attr('id'));
        var name = $(this).children('.title').text().replace(/(^\s+|\s+$)/g,'');
        $('#name').val(name);
        $('.popup-task #list').val(list);
        $('#files').remove();
        $.ajax({
            url: '/api/task/files',
            type: 'GET',
            data: "id="+id,
            success: function(data){
                $('.files-section label').after(data);
                // console.log(data);
            }
        });
        $.ajax({
            url: '/api/task/comments',
            type: 'GET',
            data: "id="+id,
            success: function(data){
                $('#comments').remove();
                $('.comments-section label').after(data);
                // console.log(data);
            }
        });
        var inst = $('.popup-task').remodal();
        inst.open();
    });
    
    $('.btn.cancel').click(function() {
        var inst = $('.popup-task').remodal();
        inst.close();
                $('#files').remove();
        // $('.popup-task').hide('fast');
        $('#name').val();
    });
    $('.btn.comment').click(function() {
        var text = $('#comment-form').val();
        var id = $(this).attr('data-taskid');
        $.ajax({
            url: '/api/comment',
            type: 'POST',
            data: "card_id="+id+"&message="+text,
            success: function(data){
                $('#comments').remove();
                $('.comments-section label').after(data);
            }
        });
    });

    $('.btn.delete').click(function() {
        var id = $(this).attr('id');
        $.ajax({
            url: '/api/task/'+id,
            type: 'DELETE',
            success: function(){
                $('.task#'+id).remove();
                $('.popup-task').hide('fast');
            }
        });

    });

    $('.btn.add').click(function() {
        var name = $('#name').val().replace(/(^\s+|\s+$)/g,'');
        var list = $('#list').val();
        var type = $(this).attr('data-type');
        var id = $(this).attr('data-taskid');
        $.ajax({
            url: '/api/task',
            type: type,
            data: "name="+name+"&list="+list+"&id="+id,
            success: function(data){
                $('#listid-'+list+' .wrap .task-container').append(data);
                $('.popup-task').hide('fast');
            }
        });
        if (type = 'PUT') {
            $('.task#'+id).remove();
        }
    });
    // var myDropzone = new Dropzone("#drop");
    // myDropzone.on("drop", function(file) {
    //     var project_id = $('h1').attr('data-project-id');
    //     file.project = project_id;
    //     console.log(file);
    //     console.log(project_id);
    //   });
    Dropzone.options.drop = {
        init: function() {
            this.on("success", function(file, response) {
                this.removeFile(file); // This line removes the preview
                var el = this.element[1];
                var id = $(el).attr('value');
                // console.log(id);
                $.ajax({
                    url: '/api/task/files',
                    type: 'GET',
                    data: "id="+id,
                    success: function(data){
                        $('.files-section label').after(data);
                        // console.log(data);
                    }
                });
            })
        }
    };

    $('.btn.upload').click(function() {
        var files = $("#one").fileUploader('getData');
        var project_id = $('h1').attr('data-project-id');
        files.forEach(function(entry) {
            // console.log(entry);
            $.ajax({
                xhr: function()
                {
                    var xhr = new window.XMLHttpRequest();
                    // прогресс загрузки на сервер
                    xhr.upload.addEventListener("progress", function(evt){
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            // делать что-то...
                            console.log(percentComplete);
                        }
                    }, false);
                    return xhr;
                },
                url: '/api/file',
                type: 'POST',
                dataType: 'text',
                data: "data="+entry.value+'&name='+entry.title+'&project_id='+project_id
            });  
        });
      
        // console.log(files);
    });
    $("#one").fileUploader();
    $('.list .wrap .task-container').sortable({
        animation: 100,
        group: 'tasks',
        // handle: ".drag",
        onEnd: function (evt) {
            syncIndex(evt.item);
            var el = $(evt.item);
            var id = el.attr('id');
            var list_id = el.closest('.list').attr('data-id');
            var name = el.text().replace(/(^\s+|\s+$)/g,'');
            $.ajax({
                url: '/api/task',
                type: 'PUT',
                data: "name="+name+"&list="+list_id+"&id="+id
            });
        },
    });




    function syncIndex(el) {
        var list = el.closest('.list');
        list_index = {};
        list_index['list_id'] = $(list).attr('data-id');
        list_index['cards'] = [];
        $(list).find('.task').each(function(i,elem) {
            var id = $(elem).attr('id');
            task = {};
            task['id'] = id; 
            task['pos'] = i;
            list_index['cards'][i] = task;
            // list_index.cards.push(task);
        });
        console.log(list_index);
        var data = JSON.stringify(list_index);
        $.ajax({
            url: '/api/list/sync',
            type: 'POST',
            dataType: 'json',
            data: 'json='+data
        });
        console.log(data);
    }








});