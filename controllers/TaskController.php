<?php
use Illuminate\Database\Capsule\Manager as DB;

class TaskController extends Controller
{
    public function createTask($request, $response, $args)
    {
        $data = $request->getParams();
        $task = Task::create($data);
        Log::write('Добавлена новая задача"'.$task->name.'"', 'task', $task->id);
        return $response->withStatus(302)->withHeader('Location', '/admin/tasks');
    }

    public function editTask($request, $response, $args)
    {
        $data = $request->getParams();
        $task = Task::find($args['id']);
        $task->update($data);
        Log::write('Изменена задача"'.$task->name.'"', 'task', $task->id);
        return $response->withStatus(302)->withHeader('Location', '/admin/tasks');
    }


    public function deleteTask($request, $response, $args)
    {
        $id = $args['id'];
        $task = Task::find($id);
        Task::destroy($id);
        Log::write('Удалена задача "'.$task->name.'"', 'task', $id);
        return $response->withStatus(302)->withHeader('Location', '/admin/tasks');
    }



}