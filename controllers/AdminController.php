<?php
use Illuminate\Database\Capsule\Manager as DB;

class AdminController extends Controller
{
    public function showMessage($request, $response, $args)
    {
        switch ($args['message']) {
            case 'no-access':
                $message = 'У вас нет доступа для этой операции';
                break;
        }
        return $this->ci->view->render($response, 'admin/message.html.twig', ['message' => $message]);
    }

    public function showLeadList($request, $response, $args)
    {
        $leads = Lead::where([
            ['created_at', '>' , '2016-10-07 10:18:21'],
            ['client', '=', 0],
            ['tags', 'NOT LIKE', '%архив%'],
        ])->get()->sortByDesc("created_at");
        //Подгружаем лиды где теги пустые
        $leads_null = Lead::where([
            ['created_at', '>' , '2016-10-07 10:18:21'],
            ['client', '=', 0],
            ['tags', '=', Null],
        ])->get()->sortByDesc("created_at");
        //Соединяем лиды
        foreach ($leads_null as $lead) {
            $leads->push($lead);
        }
        $leads = $leads->sortByDesc("created_at");
        return $this->ci->view->render($response, 'admin/leads.html.twig', ['leads' => $leads]);
    }

    public function showLeadAdd($request, $response, $args)
    {
        return $this->ci->view->render($response, 'admin/leads-add.html.twig');
    }

    public function showLeadEdit($request, $response, $args)
    {
        $id = $request->getAttribute('id');
        $records = DB::table('tags')->where('entity_id', '=', $id)->get();
        $tags = [];
        foreach ($records as $record) {
            $tags[] = $record->name;
        }
        $lead = Lead::find($id);
        return $this->ci->view->render($response, 'admin/leads-add.html.twig', ['lead' => $lead, 'tags' => $tags]);
    }

    public function showCompanyAdd($request, $response, $args)
    {
        return $this->ci->view->render($response, 'admin/company-add.html.twig');
    }

    public function showCompanyList($request, $response, $args)
    {
        return $this->ci->view->render($response, 'admin/company.html.twig', ['company' => Company::all()]);
    }

    public function showCompanyEdit($request, $response, $args)
    {
        return $this->ci->view->render($response, 'admin/company-add.html.twig', ['company' => Company::find($args['id'])]);
    }

    public function showCompanyRequisites($request, $response, $args)
    {
        $requisites = DB::table('requisites')->where('company_id', '=', $args['id'])->first();
        $company = Company::find($args['id']);
        return $this->ci->view->render($response, 'admin/requisites.html.twig', ['requisites' => $requisites, 'id' => $args['id'], 'company' => $company]);
    }

    public function showDealList($request, $response, $args)
    {
        $deals = Deal::all()->sortByDesc('created_at');
        $companies = $this->allTree(Company::all()->toArray());
        return $this->ci->view->render($response, 'admin/deal/deals.html.twig', ['deals' => $deals, 'companies' => $companies]);
    }

    public function showDealAdd($request, $response, $args)
    {
        $companies = $this->allTree(Company::all()->toArray());
        return $this->ci->view->render($response, 'admin/deal/deal-add.html.twig', ['companies' => $companies]);
    }

    public function showDealEdit($request, $response, $args)
    {
        $companies = $this->allTree(Company::all()->toArray());
        return $this->ci->view->render($response, 'admin/deal/deal-add.html.twig',
            ['deal' => Deal::find($args['id']), 'companies' => $companies]);
    }

    public function showPeopleAdd($request, $response, $args)
    {
        $companies = Company::all()->toArray();
        return $this->ci->view->render($response, 'admin/peoples-add.html.twig', ['companies' => $companies]);
    }

    public function showPeopleList($request, $response, $args)
    {
        $peoples = People::all()->toArray();
        $companies = Company::all()->toArray();
        return $this->ci->view->render($response, 'admin/peoples.html.twig', ['peoples' => $peoples, 'companies' => $companies]);
    }

    public function showPeopleEdit($request, $response, $args)
    {
        $people = People::find($args['id']);
        $companies = Company::all()->toArray();
        return $this->ci->view->render($response, 'admin/peoples-add.html.twig', ['people' => $people, 'companies' => $companies]);
    }

    public function showTaskAdd($request, $response, $args)
    {
        $lead = Lead::find($args['id']);
        $task = new Task();
        $task->lead = $lead;
        return $this->ci->view->render($response, 'admin/task/task-add.html.twig', ['task' => $task]);
    }

    public function showTaskEdit($request, $response, $args)
    {
        $task = Task::find($args['id']);
        return $this->ci->view->render($response, 'admin/task/task-add.html.twig', ['task' => $task]);
    }

    public function showTaskList($request, $response, $args)
    {
        $tasks = Task::all()->sortBy('date');
        return $this->ci->view->render($response, 'admin/task/tasks.html.twig', ['tasks' => $tasks]);
    }



    public function showIndex()
    {
        $this->slim->redirect('/admin/content');
    }

}