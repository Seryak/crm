<?php
use Illuminate\Database\Capsule\Manager as DB;

class CompanyController extends Controller
{
    public function createCompany($request, $response, $args)
    {
        $data = $request->getParams();
        $company = Company::create($data);
        Log::write('Добавлена новая Компания "'.$data['name'].'"', 'company', $company->id);
        return $response->withStatus(302)->withHeader('Location', '/admin/company');
    }

    public function editCompany($request, $response, $args)
    {
        $data = $request->getParams();
        $company = Company::find($args['id']);
        foreach ($data as $key => $value) {
            $company->$key = $value;
        }
        $company->save();
        Log::write('Изменена компания "'.$company->name.'"', 'company', $company->id);
        return $response->withStatus(302)->withHeader('Location', '/admin/company');
    }

    public function editCompanyRequisites($request, $response, $args)
    {
        $data = $request->getParams();
        $data['company_id'] = $args['id'];
        $requisites = DB::table('requisites')->where('company_id', '=', $args['id'])->get()->first();
        if ($requisites == FALSE) {
            DB::table('requisites')->insert($data);
        } else {
            DB::table('requisites')->where('company_id', '=', $args['id'])->update($data);
        }
        return $response->withStatus(302)->withHeader('Location', '/admin/company');
    }


    public function deleteLead($request, $response, $args)
    {
        $id = $args['id'];
        $lead = Lead::find($id);
        Lead::destroy($id);
        Log::write('Удален лид "'.$lead->name.'"', 'lead', $id);
        return $response->withStatus(302)->withHeader('Location', '/admin/leads');
    }



}