<?php
use Illuminate\Database\Capsule\Manager as DB;

class DealController extends Controller
{
    public function createDeal($request, $response, $args)
    {
        $data = $request->getParams();
        $data['created_at'] = date('Y-m-d G:i:s', time());
        $data['updated_at'] = $data['created_at'];
        $deal = Deal::create($data);
        Log::write('Добавлена новая сделка "'.$deal->name.'"', 'deal', $deal->id);
        return $response->withStatus(302)->withHeader('Location', '/admin/deals');
    }

    public function editDeal($request, $response, $args)
    {
        $data = $request->getParams();
        $data['updated_at'] = date('Y-m-d G:i:s', time());
        Deal::find($args['id'])->update($data);
        Log::write('Изменена сделка "'.$data['name'].'"', 'deal', $args['id']);
        return $response->withStatus(302)->withHeader('Location', '/admin/deals');
    }

    public function deleteDeal($request, $response, $args)
    {
        $deal = Deal::find($args['id']);
        Deal::destroy($args['id']);
        Log::write('Удалена сделка "'.$deal->name.'"', 'deal', $deal->id);
        return $response->withStatus(302)->withHeader('Location', '/admin/deals');
    }



}