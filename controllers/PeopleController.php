<?php
use Illuminate\Database\Capsule\Manager as DB;

class PeopleController extends Controller
{
    public function createPeople($request, $response, $args)
    {
        $data = $request->getParams();
        $people = People::create($data);
        Log::write('Добавлен новый человек "'.$people->name.'"', 'people', $people->id);
        return $response->withStatus(302)->withHeader('Location', '/admin/peoples');
    }

    public function editPeople($request, $response, $args)
    {
        $data = $request->getParams();
        People::find($args['id'])->update($data);
        Log::write('Изменен человек "'.$data['name'].'"', 'people', $args['id']);
        return $response->withStatus(302)->withHeader('Location', '/admin/peoples');
    }

    public function deletePeople($request, $response, $args)
    {
        $people = People::find($args['id']);
        People::destroy($args['id']);
        Log::write('Удален человек "'.$people->name.'"', 'people', $people->id);
        return $response->withStatus(302)->withHeader('Location', '/admin/peoples');
    }



}