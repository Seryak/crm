<?php

class Controller
{
    public $slim;
    public $twig_vars;
    public $view;
    protected $ci;

    public function __construct($container) {
        $this->ci = $container;
    }

    public function allTree($vars)
    {
        $tree = [];
        foreach ($vars as $var) {
            $tree[$var['id']] = $var;
        }
        return $tree;
    }


}