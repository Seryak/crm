<?php

class Project extends Model
{
    const table = 'projects';
    public $name;
    public $description;
    private $lists; //что бы не записывать информацию в базу при сохранении, делаем метод приватный
    

    public function __construct($data)
    {
        parent::__construct($data);
        $data_list[0]['name'] = 'Запланировано';
        $data_list[1]['name'] = 'В разработке';
        $data_list[2]['name'] = 'На проверке';
        $data_list[3]['name'] = 'Сделано';
        for ($i=0; $i < 4 ; $i++) {
            $data_list[$i]['pos'] = $i; 
        }
        $this->lists = $data_list;
    }

    //создает списки для этого проекта
    private function createLists()
    {
        foreach ($this->lists as $data) {
            $data['project_id'] = $this->id;
            $list = new Project_list($data);
            $list->save();
        }
    }

    public function save()
    {
        parent::save();
        if (isset($this->lists)) {
            $this->createLists();
        }
    }

}