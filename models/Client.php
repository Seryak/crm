<?php

class Client extends Model
{
    const table = 'client';
    public $name;
    public $birthday;
    public $created_date;
    public $phones;
    public $emails;
    public $title;
    public $company_id;
    


    public function __construct($data)
    {
        parent::__construct($data);
        $this->created_date = date("Y-m-d");
    }

    public static function compare($old, $new)
    {
//        d($old);
//        d($new);

        foreach ($old as $key => $value){
            if ($old[$key] != $new[$key]) {
                Log::write('Изменилась cвойство ('.$key.' -> '.$new[$key].') у задачи "'.$old['name'].'"', 'card', $old['id']);
            }
        }

    }
}