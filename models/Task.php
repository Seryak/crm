<?php

class Task extends Illuminate\Database\Eloquent\Model
{

    protected $guarded = [];

    public function lead()
    {
        return $this->hasOne('Lead', 'id', 'lead_id');
    }
}
