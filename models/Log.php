<?php

class Log extends Illuminate\Database\Eloquent\Model
{

    protected $table = 'log';
    public $timestamps = false;
    protected $guarded = [];

    public static function write($message, $entity = 'system', $entity_id = 0)
    {
        $record = new Log;
        $record->date = date('Y-m-d H:i:s');
        $record->message = $message;
        $record->entity = $entity;
        $record->entity_id = $entity_id;
        $record->save();
    }
}