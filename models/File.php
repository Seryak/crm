<?php 

class File extends Model
{
    public $name;
    public $card_id;
    public $project_id;
    public $path;
    public $size;
    // public $date;
    // public $type;
    
    public function __construct($file, $data)
    {
        $oldmask = umask(0);
        mkdir(__DIR__ . '/../files/'.$data['project_id'], 0777, true);
        umask($oldmask);
        $i = 0;
        $target = __DIR__ . '/../files/'.$data['project_id'].'/'.$file['name'];
        //Проверка на существование файла с таким же именем.
        while (file_exists($target)) {
            $i++;
            $filename = explode('.', $file['name']);
            $filename[0] = $filename[0].'_'.$i;
            $file['name'] = implode('.', $filename);
            $target = __DIR__ . '/../files/'.$data['project_id'].'/'.$file['name'];
        }
        move_uploaded_file($file['tmp_name'], $target);

        $this->name = $file['name'];
        $this->card_id = $data['card_id'];
        $this->project_id = $data['project_id'];
        $this->path = '/files/'.$data['project_id'].'/'.$file['name'];
        $this->size = $file['size']/1024;
        $this->save();
    }

    public function save()
    {
        $record = ORM::for_table('file')->create();
        $record->date = date('Y-m-d H:i:s');
        foreach ($this as $key => $value) {
            $record->$key = $value;
        }
        $record->save();
        $this->id = $record->id;
    }

    public static function load_files_from_card($id)
    {
        $records = ORM::for_table('file')->where('card_id', $id)->find_many();
        return $records;
    }
}
