<?php

class Comment extends Model
{
    const table = 'comments';
    public $message;
    public $author;
    public $card_id;
    public $date;

    public function __construct($data)
    {
        parent::__construct($data);
        $this->date = date('Y-m-d H:i:s');
        $this->author = $_SESSION['userData']['data']['ID'];
    }

    public static function getCommentsfromcard($card_id)
    {
        $records = ORM::for_table(self::table)->where('card_id', $card_id)->find_array();
        return $records;
    }
}
