<?php

class People extends Illuminate\Database\Eloquent\Model
{
    public $timestamps = false;
    protected $guarded = [];
}