<?php
use Cartalyst\Sentinel\Users\EloquentUser;
class User extends EloquentUser
{

    public $timestamps = false;
    protected $guarded = [];

    public function role() {
        return $this->belongsToMany('Role', 'role_users', 'user_id', 'role_id');
    }


}
