<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

//Синхронизируем позиции
$app->post('/api/list/sync', function (Request $request, Response $response) {
    $data = $request->getParams();
    $list = json_decode($data['json']);
    foreach ($list->cards as $card) {
        $record =  Card::load($card->id);
        $record->position = $card->pos;
        $record->save();
    }
});


$app->post('/api/project', function (Request $request, Response $response) {
    $data = $request->getParams();
    unset($data['id']); //Костыль. Если передать пустой id, то запись после создания вернет тоже пустой id.
    $project = new Project($data);
    $project->save();
    $data['id'] = $project->id;
    $twig = $this->view->render($response, 'ajax/project.html.twig', $data);
    Log::write('Создан проект "'.$project->name.'"', 'project', $project->id);
    return $twig;
});
$app->put('/api/project', function (Request $request, Response $response) {
    $data = $request->getParams();
    $project = Project::load($data['id']);
    foreach ($data as $key => $value) {
        $project->$key = $value;
    }
    $project->save();
    $twig = $this->view->render($response, 'ajax/project.html.twig', $data);
    return $twig;
    //Нужен наблюдатель
    Log::write('Изменен проект "'.$project->name.'"', 'project', $project->id);
});

$app->post('/api/file', function (Request $request, Response $response) {
    $data = $request->getParams();
    $file = new File($_FILES['file'], $data);
    ddd($file);
});

//Создаем комментарий
$app->post('/api/comment', function (Request $request, Response $response) {
    $data = $request->getParams();
    $comment = new Comment($data);
    $comment->save();
    // ddd($comment);
    Log::write('Комментарий "'.$comment->message.'"', 'card', $comment->card_id);
    $comments = Comment::getCommentsfromcard($comment->card_id);
    $users = User::getAll();
    $twig = $this->view->render($response, 'ajax/comments.html.twig', ['comments' => $comments, 'users' => $users]);
    return $twig;
});










$app->post('/api/login', 'UserController:doLogin');
$app->get('/api/logout', 'UserController:doLogout');



// Лиды
$app->post('/api/lead', 'LeadController:createLead');
$app->post('/api/lead/{id}', 'LeadController:editLead');
$app->get('/api/delete/lead/{id}', 'LeadController:deleteLead')->add( new IsAdmin($container) );
//Сделки
$app->post('/api/deal', 'DealController:createDeal');
$app->post('/api/deal/{id}', 'DealController:editDeal');
$app->get('/api/delete/deal/{id}', 'DealController:deleteDeal')->add( new IsAdmin($container) );
//Компании
$app->post('/api/company', 'CompanyController:createCompany');
$app->post('/api/company/{id}', 'CompanyController:editCompany');
$app->post('/api/company/{id}/requisites', 'CompanyController:editCompanyRequisites');
//Люди
$app->post('/api/people', 'PeopleController:createPeople');
$app->post('/api/people/{id}', 'PeopleController:editPeople');
$app->get('/api/delete/people/{id}', 'PeopleController:deletePeople')->add( new IsAdmin($container) );
//Задачи
$app->post('/api/task', 'TaskController:createTask');
$app->post('/api/task/{id}', 'TaskController:editTask');


$app->get('/message/{message}', 'AdminController:showMessage');
