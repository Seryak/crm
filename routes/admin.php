<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/admin/projects', function (Request $request, Response $response) {
    $projects = ORM::for_table('projects')->find_many();
    $twig = $this->view->render($response, 'admin/projects.html.twig', ['projects' => $projects ]);
    return $twig;
});
$app->get('/admin/project/{id}', function (Request $request, Response $response) {
    $id = $request->getAttribute('id');
    $project = ORM::for_table('projects')->find_one($id);
    $twig = $this->view->render($response, 'admin/project.html.twig', ['project' => $project ]);
    return $twig;
});
$app->get('/admin/users', function (Request $request, Response $response) {
    $twig = $this->view->render($response, 'admin/users.html.twig');
    return $twig;
});


// Лиды
$app->get('/admin/lead', 'AdminController:showLeadAdd');
$app->get('/admin/leads', 'AdminController:showLeadList');
$app->get('/admin/lead/{id}', 'AdminController:showLeadEdit');
// Компании
$app->get('/admin/company/add', 'AdminController:showCompanyAdd');
$app->get('/admin/company', 'AdminController:showCompanyList');
$app->get('/admin/company/{id}', 'AdminController:showCompanyEdit');
$app->get('/admin/company/{id}/requisites', 'AdminController:showCompanyRequisites');
//Сделки
$app->get('/admin/deals', 'AdminController:showDealList');
$app->get('/admin/deal', 'AdminController:showDealAdd');
$app->get('/admin/deal/{id}', 'AdminController:showDealEdit');
//Люди
$app->get('/admin/people', 'AdminController:showPeopleAdd');
$app->get('/admin/peoples', 'AdminController:showPeopleList');
$app->get('/admin/people/{id}', 'AdminController:showPeopleEdit');
//Задачи
$app->get('/admin/lead/{id}/task', 'AdminController:showTaskAdd');
$app->get('/admin/task/{id}', 'AdminController:showTaskEdit');
$app->get('/admin/tasks', 'AdminController:showTaskList');
