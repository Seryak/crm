<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response) {
    return $this->view->render($response, 'layout.html.twig');
});
$app->get('/login', 'UserController:showLoginPage');

$app->get('/project/{id}', function (Request $request, Response $response) {
    $id = $request->getAttribute('id');
    $project = ORM::for_table('projects')->find_one($id);
    $records = ORM::for_table('lists')->where('project_id', $id)->find_many();
    foreach ($records as $record)
    {
        $cards = ORM::for_table('cards')->where('list_id', $record->id)->order_by_asc('position')->find_many();
        $lists[$record->id]['name'] = $record->name;
        $lists[$record->id]['tasks'] = $cards;
        $lists[$record->id]['id'] = $record->id;
    }
    d($_SESSION);
    return $this->view->render($response, 'project.html.twig', ['project' => $project, 'lists' => $lists]);
});

$app->get('/project/{id}/log', function (Request $request, Response $response) {
    $id = $request->getAttribute('id');
    $project = ORM::for_table('projects')->find_one($id);
    $lists = ORM::for_table('lists')->where('project_id', $id)->find_many();
    $cards = [];
    foreach ($lists as $list)
    {
        $records = ORM::for_table('cards')->where('list_id', $list->id)->find_many();
        foreach ($records as $record) {
            array_push($cards, $record);
        }
    }
    $logs = [];
    foreach ($cards as $card) {
        $records = ORM::for_table('log')->where('entity_id', $card->id)->order_by_asc('date')->find_many();
        foreach ($records as $record) {
            array_push($logs, $record);
        }
    }
    function cmp($a, $b) {
        if ($a->date == $b->date) {
            return 0;
        }
        return ($a->date < $b->date) ? -1 : 1;
    }
    uasort($logs, 'cmp');
//     d($logs);
    return $this->view->render($response, 'project-log.html.twig', ['project' => $project, 'logs' => $logs]);
});


$app->post('/proverka', function (Request $request, Response $response) {
    $text = "Какой-то текст";
    $fp = fopen("file.txt", "w");
    fwrite($fp, $text);
    fclose($fp);
});

$app->get('/hook', function (Request $request, Response $response) {
    $text = "Какой-то текст";
    $fp = fopen("file.txt", "w");
    fwrite($fp, $text);
    fclose($fp);
});